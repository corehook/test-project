Rails.application.routes.draw do

  scope :csv do
    match 'export' => 'contacts#export_csv', via: [:get, :post], as: :csv_export
    match 'import' => 'contacts#import_csv', via: [:get, :post], as: :csv_import
  end

  resources :contacts, path: '/' do

    match 'share' => 'contacts#share', via: [:get, :post], as: :share

    resource :phones
    resource :emails

  end



end
