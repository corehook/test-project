class ContactMailer < ApplicationMailer

  def share_contact(contact, recipient)
    @contact = contact
    mail(to: recipient, subject: t('global.contact_shared'))
  end

end

