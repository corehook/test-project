class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action {
    flash[:error] ||= []
    flash[:notice] ||= []
  }

  #
  # type = :error or :notice
  # message = message
  def flash_it(type, message)
    flash[type] << message
  end
end
