class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy, :share]

  #
  # Export/Import CSV
  def export_csv
    @contacts = Contact.all

    respond_to do |format|
      format.csv { send_data @contacts.to_csv ,  filename: "contacts-#{Date.today}.csv"  }
    end
  end

  def import_csv
    @contacts_count = {imported: 0, updated: 0}
    if request.post?
      file = csv_as_data(params)

      @contacts_count = Contact.import(file)
    end
  end

  #
  # Share
  def share
    ContactMailer.share_contact(@contact, params[:contact][:recipient]).deliver_later
    redirect_to :back
  end

  def index
    @contacts = Contact.all
  end

  def create
    @contact = Contact.new(contact_params)

    if check_contact_params and @contact.save
      @contact.emails.create(email_params)
      @contact.phones.create(phone_params)

      flash_it(:notice, t('success.contact_successfully_created'))
    else
      flash_it(:error, t('errors.phone_and_email_must_be_presented'))
    end

    respond_to do |format|
      if not @contact.new_record?
        format.html { redirect_to @contact}
      else
        format.html { render action: 'new', notice: flash[:notice], error: flash[:error] }
      end
    end

  end

  def new
    @contact = Contact.new
  end

  def edit
  end

  def show
  end

  def update
    if request.patch? and @contact
      if @contact.update_attributes(contact_params)
        flash_it(:notice, t('success.contact_updated'))
      else
        flash_it(:error, t('errors.cant_update_contact'))
        flash[:error] += @contact.errors.full_messages
      end
    end

    redirect_to contact_path(@contact)
  end

  def destroy
    if @contact.destroy
      flash_it(:notice, t('success.contact_successfully_destroyed'))
    end
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: flash[:notice], error: flash[:error] }
    end
  end


  private


  def csv_as_data(params)
    if params.has_key? :contact and params[:contact].has_key? :csv_file
      csv_file_param = params.require(:contact).permit(:csv_file)
      return csv_file_param[:csv_file]
    end
    return nil
  end
  def set_contact
    @contact = Contact.find(params[:id]) if params.has_key? :id
    @contact = Contact.find(params[:contact_id]) if params.has_key? :contact_id
  end

  def check_contact_params
    return false if not phone_params.has_key? :number or phone_params[:number].length <= 0
    return false if not email_params.has_key? :email  or email_params[:email].length <= 0

    return true
  end

  def contact_params
    params.require(:contact).permit(:first_name, :last_name)
  end

  def phone_params
    params.require(:contact).require(:phone).permit(:number)
  end

  def email_params
    params.require(:contact).require(:email).permit(:email)
  end

end
