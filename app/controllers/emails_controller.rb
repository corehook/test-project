class EmailsController < ApplicationController
  before_action :set_email

  def index
  end

  def create
    if @contact
      @email = @contact.emails.create(email_params)
      if @email and not @email.new_record?
        flash_it(:notice, t('success.email_created'))
      end
    end

    respond_to do |format|
      if not @email.new_record?
        format.html { redirect_to @contact}
      else
        format.html { render action: 'new', notice: flash[:notice], error: flash[:error] }
      end
    end
  end

  def new
    @email = @contact.emails.new()
  end

  def edit
  end

  def show
  end

  def update
  end

  def destroy
    if @contact.emails.count >= 2
      if @email.destroy
        flash_it(:notice,t('success.email_destroyed'))
      end
    else
      flash_it(:error, t('errors.each_contact_should_have_at_least_one_email'))
    end


    respond_to do |format|
      format.html { redirect_to contact_path(@contact), notice: flash[:notice], error: flash[:error]}
    end
  end


  private

  def set_email
    if params.has_key? :contact_id
      @contact = Contact.find(params[:contact_id])
      @email = Email.find_by(id: params[:id], contact_id: params[:contact_id])
    end
  end

  def email_params
    params.require(:email).permit(:contact_id, :email)
  end
end
