class PhonesController < ApplicationController
  before_action :set_phone

  def index
  end

  def create
    if @contact
      @phone = @contact.phones.create(phone_params)
      if @phone and not @phone.new_record?
        flash_it(:notice, t('success.phone_created'))
      end
    end

    respond_to do |format|
      if not @phone.new_record?
        format.html { redirect_to @contact}
      else
        format.html { render action: 'new', notice: flash[:notice], error: flash[:error] }
      end
    end
  end

  def new
    @phone = Phone.new(contact_id: @contact.id)
  end

  def edit
  end

  def show
  end

  def update
  end

  def destroy
    if @contact.phones.count >= 2
      if @phone.destroy
        flash_it(:notice,t('success.phone_destroyed'))
      end
    else
      flash_it(:error,t('errors.each_contact_should_have_at_least_one_phone'))
    end


    respond_to do |format|
      format.html { redirect_to contact_path(@contact), error: flash[:error], notice: flash[:notice]}
    end
  end

  private

  def set_phone
    @contact = Contact.find(params[:contact_id])
    @phone = Phone.find_by(id: params[:id], contact_id: params[:contact_id])
  end


  def phone_params
    params.require(:phone).permit(:contact_id, :number)
  end
end
