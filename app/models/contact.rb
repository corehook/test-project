class Contact < ActiveRecord::Base

  has_many :phones, :dependent => :destroy
  has_many :emails, :dependent => :destroy
  attr_accessor :recipient
  validates :first_name, uniqueness: { scope: :last_name, message: I18n.t('errors.first_last_names_must_be_unique')}

  before_save :check_name_unique

  def name
    "#{first_name} #{last_name}"
  end

  def self.import(file)
    result = {imported: 0, updated: 0}
    begin
      csv_data = file.read
      if csv_data.length > 0
        csv_rows = CSV.parse(csv_data).rotate
        csv_rows.each do |contact_csv_data|
          contact_id = contact_csv_data[0]
          unless contact_id.eql? 'id'
            contact = find_by_id(contact_id)
            contact_data = extract_csv_data(contact_csv_data)
            if contact
              result[:updated] += 1
              contact.update_attributes(contact_data.slice(:first_name, :last_name))
              contact.phones.find_or_create_by(number: contact_data[:phone])
              contact.emails.find_or_create_by(email: contact_data[:email])
            else
              result[:imported] += 1
              contact = Contact.create(contact_data.slice(:first_name, :last_name))
              if contact
                contact.phones.find_or_create_by(number: contact_data[:phone])
                contact.emails.find_or_create_by(email: contact_data[:email])
              end
            end
          end

        end
      end
    rescue Exception => e
      logger.info "Exception #{e.inspect}"
    end

    return result
  end

  def self.extract_csv_data(raw_data)
    return {first_name: raw_data[1], last_name: raw_data[2], phone: raw_data[5], email: raw_data[6]} if raw_data.size >= 7
    return {}
  end

  def self.to_csv(options = {})
    begin
      CSV.generate(options) do |csv|
        csv << ["id", "first_name", "last_name", "created_at", "updated_at", "phone", "email"]
        logger.info "#{csv}"
        all.each do |contact|
          values = contact.attributes.values_at(*column_names)
          values << contact.phones.first.number
          values << contact.emails.first.email
          csv << values
        end
      end
    rescue Exception => e
      logger.info "Exception #{e.inspect}"
    end

  end

  def check_name_unique
    if Contact.where(first_name: first_name, last_name: last_name).length > 0
      errors.add(:base, I18n.t('errors.first_last_names_must_be_unique'))
      return false
    end
    return true
  end

end
