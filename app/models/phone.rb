class Phone < ActiveRecord::Base
  belongs_to :contact
  validates_presence_of :number
end
